module.exports = {
  markdown: {
    lineNumbers: true
  },
  serviceWorker: true,
  head: [["link", { rel: "icon", href: "/favicon.png" }]],
  dest: "dist",
  locales: {
    "": {
      lang: "zh-CN",
      title: "前端文档",
      description: "f2e document"
    }
  },
  ga: "UA-147153467-1",
  themeConfig: {
    // sidebar: "auto",
    sidebar: {
      "/guide/": [
        {
          collapsable: false,
          sidebarDepth: 2,
          children: ["new", "h5", "admin"]
        }
      ],
      "/drone/": [
        {
          collapsable: false,
          sidebarDepth: 3,
          children: ["", "plugin"]
        }
      ]
    },
    nav: [
      {
        text: "指南",
        items: [
          { text: "新人指南", link: "/guide/new" },

          { text: "后台组指南", link: "/guide/admin" },
          { text: "H5 组指南", link: "/guide/h5" }
        ]
      },
      {
        text: "说明文档",
        items: [{ text: "Drone CI", link: "/drone/" }]
      }
    ],
    lastUpdated: "最后更新",
    repo: "https://gitlab.pingfang.com/f2e/doc",
    repoLabel: "查看 Gitlab"
    // editLinks: true,
    // editLinkText: '编辑文档',
  }
};


# 插件

- [ding-talk](https://gitlab.pingfang.com/drone-plugin/ding-talk)
- [scp](https://gitlab.pingfang.com/drone-plugin/scp)
- [npm-publish](https://gitlab.pingfang.com/drone-plugin/npm-publish)
- [node](https://gitlab.pingfang.com/drone-plugin/node)
- [yarn](https://gitlab.pingfang.com/drone-plugin/yarn)
- ansible

<br>

## ding-talk
钉钉通知插件

![](/drone/ding-talk.jpg)

### 功能
- 展示构建信息
- 展示提交信息
- 自定义消息
- 多群推送
- @ 个人
- @ 所有人
- 查看构建
- 查看提交

### 使用
[查看仓库 readme](https://gitlab.pingfang.com/drone-plugin/ding-talk)

<br>

## scp
scp 命令插件

![](/drone/scp.png)

### 功能
上传文件到指定服务器

### 使用
[查看仓库 readme](https://gitlab.pingfang.com/drone-plugin/scp)

<br>

## npm-publish
npm 包推送插件
![](/drone/npm-publish.png)

### 功能
上传库到[私有仓库](http://npm.pf.com/#/)

### 使用
[查看仓库 readme](https://gitlab.pingfang.com/drone-plugin/npm-publish)

<br>

## node
node 基础镜像

### 功能
包含 node、npm、yarn、cnpm

### 使用
[查看仓库 readme](https://gitlab.pingfang.com/drone-plugin/node)

npm、yarn、cnpm 默认使用 [https://registry.npm.taobao.org](https://registry.npm.taobao.org) 源

<br>

## yarn
yarn 基础镜像

### 功能
包含 yarn，默认使用 [https://registry.npm.taobao.org](https://registry.npm.taobao.org) 源

### 使用
[查看仓库 readme](https://gitlab.pingfang.com/drone-plugin/yarn)

<br>

## ansible
ansible 容器由运维维护，用来部署代码到测试服务器

不同分支发到不同的环境，例如 t 分支会部署代码至环境一，t2 分支会部署代码至环境二

### 使用
```yaml
- name: deploy
  image: reg.pingfang.com/drone-plugin/ansible
  pull: always
```
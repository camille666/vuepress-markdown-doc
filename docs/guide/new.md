# 新人指南

办公软件安装
1. 通信软件：钉钉
2. 邮件管理：推荐foxmail
3. chrome

开发软件安装

1. iterm2：终端利器最好用
2. vscode：

安装Homebrew（mac上比较好用的一个软件安装工具）
运行下面命令安装brew，如果出现安装失败，可以看下是不是存储空间问题或者git config下载大小的限制
```shell
/usr/bin/ruby -e “$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
```
安装需要等待一小段时间
安装node（选择nvm安装，方便版本管理）
首先安装nvm，node版本管理工具
```shell
brew install nvm
```
通过nvm安装node.js
`nvm install stable` 或 `nvm install vs`指定版本
此时npm也同时安装好了
git
如果机器上没有git，自行安装就行
```shell
brew install git
```

没有权限找老大或者其他小伙伴加一下。

## git工作流
1. 从develop分支拉功能或者修复分支，命名规则分别为`feature/xxx`，`hotfix/yyy`
2. 在本地分支进行开发
3. 发布到测试环境，把分支和到测试环境，一般为`t`开头，推送到远端，然后进行项目发布
4. 上灰度，将分支合并到gray分支，把接口截图发给有灰度发布权限的小伙伴，并告知发布
5. 上线，在gitlab提交merge request（feature分支到develop分支），并告知有发布权限的小伙伴进行代码review和发布
# 管理后台

* 测试环境: https://admin.t.pingfang.com/list
    * 登录：
        * debug: https://authapi.pingfang.com/login/setCookie/id/2
        * 钉钉扫码：权限找 @小红
* 正式环境：https://admin.pingfang.com/
    * 钉钉扫码登录，权限找 @小明

## 开发相关
### 项目启动
#### 1. 安装依赖
npm install
#### 2. 启动项目
npm start
#### 3. 本地开启跨域
使用chrome插件或者命令行 `open -a "Google Chrome" --args --disable-web-security  --user-data-dir`

### 打包模式
打包模式分为2种:（上线时请选择性能模式，线上代码性能是第一位，）
极速模式下，电脑运行流畅的情况下打包时间为47s左右，性能模式下在68s左右
1. 极速模式 开启externals，打包比较快，但是页面的载入会稍慢,但是打包以及编译时对电脑的性能消耗较小  本地开发 npm run start 线上打包 npm run build
2. 性能模式 关闭externals，页面的载入会比较快，但是打包稍慢，但是打包以及编译时对电脑的性能消耗较大 本地开发 npm run start:performance 线上打包 npm run build:performance
建议：本地开发使用极速模式，发布测试环境也使用极速模式，上线时使用性能模式
 

    
